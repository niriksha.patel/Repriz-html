if (!("ontouchstart" in document.documentElement)) {
    document.documentElement.className += " no-touch";
} else {
    document.documentElement.className += " touch";
}
(function($) {
    /*Initializing START*/
    $(document).ready(function() {
        

        $('.menu-icon').on('click',function(){
            $(this).css('z-index','10000');
            $(this).find('.top-line').toggleClass('top-line-rotate');
            $(this).find('.bot-line').toggleClass('bot-line-rotate');
            $(this).find('.mid-line').toggle();
            $('.left-nav').toggleClass('open');
            $('.overlay').fadeToggle();
        });
        $('.overlay').on('click', function(){
            $('.left-nav').removeClass('open');
            $('.top-line').removeClass('top-line-rotate');
            $('.bot-line').removeClass('bot-line-rotate');
            $('.mid-line').fadeIn();
            $(this).fadeOut();
        });

        $('.search-btn-icon').on('click', function(){
            $('.header-search').slideToggle('fast');
        });

        $('.custom-checkbox label a').on("click", function( event, data ){
            event.stopPropagation();
            $(this).attr('href');
         });

        $("body").popover({ selector: '[data-toggle=popover]', trigger: "hover" });
        
        $('#saleSlider').slick({
            slidesToShow: 2,
            slidesToScroll: 1,
            arrows: true,
            infinite: true,
            responsive: [
            {
                breakpoint: 992,
                settings: {
                    arrows: false,
                    dots: true,
                    infinite: false
                }
            },
            {
                breakpoint: 767,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    arrows: false,
                    dots: true,
                    infinite: false
                }
            }]
        });

        $('#investorSlider').slick({
            slidesToShow: 2,
            slidesToScroll: 1,
            arrows: true,
            infinite: true,
            responsive: [
            {
                breakpoint: 992,
                settings: {
                    arrows: false,
                    dots: true,
                    infinite: false
                }
            },
            {
                breakpoint: 767,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    arrows: false,
                    dots: true,
                    infinite: false
                }
            }]
        });

        $('a[href="#"]').click(function(e) {
            e.preventDefault();
        });

        $('.bookmark-btn').on('click', function(){
            if ($(this).find('span').hasClass('heart-icon-empty')) {
                $(this).find('span').removeClass('heart-icon-empty').addClass('heart-icon-full');
            }
            else
                $(this).find('span').removeClass('heart-icon-full').addClass('heart-icon-empty');
        });

        $('#getCreditBtn').on('click', function(e){
            e.preventDefault();
            $('.alert-credit-ex').show();
        });

        $(".get-year").text( (new Date).getFullYear() );
        
        



    });
    $(window).scroll(function(){
        var scroll = $(window).scrollTop();
        var winHeight = $(window).height();
        var bottomPos = scroll + winHeight;
        if (bottomPos < 3750 && scroll > 250) {
            $('.faq-main').addClass('sticky');
        }
        else {
            $('.faq-main').removeClass('sticky');
        }
    }).scroll();

    $('#askQuesBtn').on('click', function(){
        $('.ask-popup').animate({ height: 500 }, 500);
    });
    $('.ask-popup .close-btn').on('click', function(e){
        e.preventDefault();
        $('.ask-popup').animate({ height: 0 }, 500);
    });


    /* Filter Accotdion */
    $('.filter-panel-heading').on('click', function(){
        if ($(this).next('.filter-block').css('display') == 'none') {
            $(this).addClass('open');
            $(this).next('.filter-block').slideDown();
        }
        else{
            $(this).removeClass('open');
            $(this).next('.filter-block').slideUp();
        }
    });
    $('#filterBtn').on('click', function(){
        $('.filters').animate({ 'right':'0' });
    });
    $('.close-filter').on('click', function(){
        $('.filters').animate({ 'right':'-100%' });
    });

    
    $(window).on('load', function() {
        console.log('window loaded successfully');
        new WOW().init();
    });
    $(window).on('resize', function() {
        console.log('window resized to ' + $(window).innerWidth() + '');
    });
})(jQuery);
/*Initializing END*/