/* File: gulpfile.js */
/*Gupt module*/
var gulp = require('gulp');

/*JShint modules*/
var jshint = require('gulp-jshint');

/*JS minify modules*/
var sourcemaps = require('gulp-sourcemaps');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var gutil = require('gulp-util');
var minify = require('gulp-minify');
var gulpIgnore = require('gulp-ignore');
var fs = require("fs");
var path = require('path');

/* HTML minify Modules */
var minifyHTML = require('gulp-minify-html');

/*Image minify module*/
var changed = require('gulp-changed');
var imagemin = require('gulp-imagemin');

/* CSS Minify Modules */
var autoprefix = require('gulp-autoprefixer');
var minifyCSS = require('gulp-minify-css');

/* saas compilation */
var sass = require('gulp-sass');
var compass = require('gulp-compass')
var sourcemaps = require('gulp-sourcemaps');


// define the default task and add the watch task to it
gulp.task('default', ['watch','js-libs','css-libs','imagemin']);

/*########################
# JavaScript Tasks #
#########################*/

gulp.task('jshint-one', function() {
    return gulp.src('assets/js/general.js')
        .pipe(jshint())
        .pipe(jshint.reporter('jshint-stylish'));

});

/* Jshint task*/
gulp.task('jshint', function() {
    return gulp.src('assets/js/*.js')
        .pipe(jshint())
        .pipe(jshint.reporter('jshint-stylish'));

});

/* JS Minify task*/
gulp.task('js-libs', function() {
    gulp.src([
            "assets/vendor/jquery/dist/jquery.min.js",
            "assets/vendor/bootstrap/dist/js/bootstrap.min.js"
        ])
        .pipe(sourcemaps.init())
        .pipe(concat('javascript.lib.js'))
        //only uglify if gulp is ran with '--type production'
        //.pipe(1 ? uglify() : gutil.noop())
        .pipe(minify())
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('assets/js'));
});

/*########################
# HTML Tasks #
#########################*/
/*W3c HTML validation*/
var w3cjs = require('gulp-w3cjs');
var through2 = require('through2');
 
gulp.task('html-v', function () {
    gulp.src('*.html')
        .pipe(w3cjs())
        .pipe(through2.obj(function(file, enc, cb){
            cb(null, file);
            if (!file.w3cjs.success){
                throw new Error('HTML validation error(s) found');
            }
        }));
});

/*########################
# CSS Tasks #
#########################*/
/* Merging and minifying min css */
gulp.task('css-libs', function() {
  gulp.src([
    'assets/vendor/bootstrap/dist/css/bootstrap.min.css',
    'assets/vendor/bootstrap/dist/css/bootstrap-theme.min.css'
    ])
    .pipe(concat('styles-lib-min.css'))
    .pipe(autoprefix('last 2 versions'))
    .pipe(minifyCSS())
    .pipe(gulp.dest('assets/css'));
});
/* Sass file compilation */
gulp.task('sass', function () {
  return gulp.src('assets/scss/*.scss')
    //.pipe(sourcemaps.init())
    .pipe(sass({outputStyle: 'compact'}).on('error', sass.logError)) /* expanded / compact / compressed*/
    //.pipe(sass.sync().on('error', sass.logError))
    //.pipe(sourcemaps.write())
    .pipe(gulp.dest('assets/css'));
});
/* Sass file compilation */
gulp.task('compass', function() {
  gulp.src('assets/scss/*.scss')
    .pipe(compass({
      css: 'assets/css',
      sass: 'assets/scss',
      image: 'assets/images'
    }))
    .pipe(minifyCSS())
    .pipe(gulp.dest('assets/css'));
});
/* W3c css validation */
var cssvalidate = require('gulp-w3c-css');
gulp.task('css-v', function() {
  gulp.src('assets/css/styles.css')
  .pipe(cssvalidate())
  .pipe(gulp.dest('assets/css/csserr'));
});


  

/*########################
# Images Tasks #
#########################*/
/* Images minigy task*/
gulp.task('imagemin', function() {
    var imgSrc = 'assets/images/*',
        imgDst = 'assets/img';

    gulp.src(imgSrc)
        .pipe(changed(imgDst))
        .pipe(imagemin())
        .pipe(gulp.dest(imgDst));
});


// configure which files to watch and what tasks to use on file changes
gulp.task('watch', function() {
    gulp.watch('assets/css/*.css', ['css-libs']);
    gulp.watch('assets/images/*', ['imagemin']);
    //gulp.watch('*.html', ['html-v']);
});